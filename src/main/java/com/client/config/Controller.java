package com.client.config;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class Controller {

    @Value("${spring.application.name}")
    private String value;

    @GetMapping("/value")
    public String getStringFromResources() {
        System.out.println(value);
        return value;
    }
}
